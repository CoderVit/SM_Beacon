﻿//using DirWatcher.Infrastructure.Models;
//using Microsoft.EntityFrameworkCore;

//namespace DirWatcher.Infrastructure.DataBase
//{
//    public class BeaconDbContext : DbContext
//    {
//        public BeaconDbContext(DbContextOptions options) : base(options) { }

//        public DbSet<GisDevice> GisDevices { get; set; }
//        public DbSet<ShipDevice> ShipDevices { get; set; }
//        public DbSet<Device> Devices { get; set; }
//        public DbSet<Message> Messages { get; set; }
//        public DbSet<Coordinate> Coordinates { get; set; }

//        protected override void OnModelCreating(ModelBuilder modelBuilder)
//        {
//            modelBuilder.Entity<GisDevice>().HasKey(c => new { c.Id, c.DeviceId });

//            modelBuilder.Entity<Message>().HasKey(c => new { c.CoordinateTime, c.DeviceId });
//            modelBuilder.Entity<Coordinate>().HasKey(c => new { c.DeviceId, c.Time });
//            modelBuilder.Entity<GisDevice>().HasOne(s => s.Device).WithOne(device => device.GisDevice).HasForeignKey<GisDevice>(device => device.DeviceId);
//            base.OnModelCreating(modelBuilder);
//        }
//    }
//}