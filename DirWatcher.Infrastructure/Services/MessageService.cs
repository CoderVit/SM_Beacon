﻿//using System;
//using System.Linq;
//using System.Threading.Tasks;
//using DirWatcher.Infrastructure.DataBase;
//using DirWatcher.Infrastructure.Models;
//using DirWatcher.SatShipLib.Models;
//using Microsoft.EntityFrameworkCore;
//using StxLib.Abstract;
//using StxLib.Converters;
//using StxLib.Models.Xml;

//namespace DirWatcher.Infrastructure.Services
//{
//    public class MessageService
//    {
//        private readonly BeaconDbContext m_dbContext;

//        private const byte SHIP_SAT_PROTOCOL = 3;
//        private const byte FTP_MESSAGE_TYPE = 2;

//        private readonly IByteConverter m_byteConverter = new StxByteConverter();

//        public MessageService(BeaconDbContext dbContext)
//        {
//            m_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
//        }

//        public async Task AddMessage(StuMessage message, SatShipMessage satShipMessage)
//        {
//            var id = await m_dbContext.Devices.Where(device => device.DeviceId == message.DeviceId).Select(device => device.Id).SingleAsync();

//            await m_dbContext.Messages.AddAsync(new Message
//            {
//                MessageTime = satShipMessage.MessageTime,
//                CoordinateTime = satShipMessage.Coordinates[0].Time,
//                DeviceId = id,
//                Hex = m_byteConverter.ToBytes(message.Payload.Value),
//                ProtocolId = SHIP_SAT_PROTOCOL,
//                MessageTypeId = FTP_MESSAGE_TYPE
//            });

//            foreach (var coordinate in satShipMessage.Coordinates)
//            {
//                await m_dbContext.Coordinates.AddAsync(new Coordinate
//                {
//                    DeviceId = message.DeviceId,
//                    Time = coordinate.Time,
//                    Latitude = coordinate.Latitude,
//                    Longitude = coordinate.Longitude,
//                    NW = $"{coordinate.LatitudeSign}{coordinate.LongitudeSign}",
//                    Temperature = satShipMessage.Temperature,
//                    Battery = (int)satShipMessage.Battery,
//                    IsAlert = false,
//                    IsValid = true
//                });
//            }

//            await m_dbContext.SaveChangesAsync();
//        }
//    }
//}