﻿//using System;
//using System.Threading.Tasks;
//using DirWatcher.Infrastructure.DataBase;
//using Microsoft.EntityFrameworkCore;

//namespace DirWatcher.Infrastructure.Services
//{
//    public class DeviceService
//    {
//        private readonly BeaconDbContext m_dbContext;

//        public DeviceService(BeaconDbContext dbContext)
//        {
//            m_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
//        }

//        public async Task<(string deviceName, long floatId)> GetDeviceAsync(string deviceId)
//        {
//            var device = await m_dbContext.Devices.Include(dev => dev.GisDevice).SingleAsync(dev => dev.DeviceId == deviceId);
//            return (device.DeviceName, device.GisDevice.FloatId);
//        }
//    }
//}