﻿//using System;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

//namespace DirWatcher.Infrastructure.Models
//{
//    [Table("tblCoordinateSatellite")]
//    public class Coordinate
//    {
//        [Key]
//        public string DeviceId { get; set; }
//        [Key]
//        public DateTime Time { get; set; }
//        public double Latitude { get; set; }
//        public double Longitude { get; set; }
//        public string NW { get; set; }
//        public int Temperature { get; set; }
//        public int Battery { get; set; }
//        public bool IsValid { get; set; }
//        public bool IsAlert { get; set; }
//    }
//}