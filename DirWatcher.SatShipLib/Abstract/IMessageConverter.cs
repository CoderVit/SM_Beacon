﻿using DirWatcher.SatShipLib.Models;
using DirWatcher.SatShipLib.Models.Bits;

namespace DirWatcher.SatShipLib.Abstract
{
    public interface IMessageConverter
    {
        /// <summary>
        /// Convert to "ship" message
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="unixTime">xml message unix time</param>
        /// <returns></returns>
        SatShipMessage ToShipMessage(byte[] bytes, uint unixTime);
    }
}