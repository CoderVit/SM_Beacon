﻿using DirWatcher.SatShipLib.Models.Bits;

namespace DirWatcher.SatShipLib.Abstract
{
    public interface ISatShipConverter
    {
        BitMessage ToBitMessage(byte[] hexBytes);
    }
}