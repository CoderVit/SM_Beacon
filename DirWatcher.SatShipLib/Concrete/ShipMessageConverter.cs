﻿using System;
using DirWatcher.SatShipLib.Abstract;
using DirWatcher.SatShipLib.Models;
using StxLib.Abstract;

namespace DirWatcher.SatShipLib.Concrete
{
    public class ShipMessageConverter : IMessageConverter
    {
        private readonly ISatShipConverter m_satShipConverter;
        private readonly IBatteryConverter m_batteryConverter;
        private readonly ITime m_timeConverter;
        private readonly ICoordinateConvertor m_coordinateConvertor;

        public ShipMessageConverter(ISatShipConverter satShipConverter, IBatteryConverter batteryConverter, ITime timeConverter, ICoordinateConvertor coordinateConvertor)
        {
            m_satShipConverter = satShipConverter ?? throw new ArgumentNullException(nameof(satShipConverter));
            m_batteryConverter = batteryConverter ?? throw new ArgumentNullException(nameof(batteryConverter));
            m_timeConverter = timeConverter ?? throw new ArgumentNullException(nameof(timeConverter));
            m_coordinateConvertor = coordinateConvertor ?? throw new ArgumentNullException(nameof(coordinateConvertor));
        }

        public SatShipMessage ToShipMessage(byte[] bytes , uint unixTime)
        {
            var message = m_satShipConverter.ToBitMessage(bytes);

            var title = message.Title;

            var shipMessage = new SatShipMessage
            {
                Temperature = (byte)(title.Temperature - 64),
                Battery = m_batteryConverter.ToVoltage(title.Battery), 
                MessageTime = m_timeConverter.GetMajorCoodrinateTime(title.MajorTime, unixTime)
            };

            var coordinates = new ShipCoordinate[3];
            var coordinateUnixTime = m_timeConverter.GetUnixTime(shipMessage.MessageTime);

            for (byte coordinateIndex = 0; coordinateIndex < message.BitCoordinates.Count; coordinateIndex++)
            {
                var bitCoordinate = message.BitCoordinates[coordinateIndex];
                coordinateUnixTime -= bitCoordinate.DTime;

                coordinates[coordinateIndex] = new ShipCoordinate
                {
                    Time = m_timeConverter.GetTime(coordinateUnixTime),
                    Longitude = m_coordinateConvertor.ToLongitude(bitCoordinate.Longitude),
                    LongitudeSign = bitCoordinate.LongitudeSign ? 'W' : 'E',
                    Latitude = m_coordinateConvertor.ToLatitude(bitCoordinate.Latitude),
                    LatitudeSign = bitCoordinate.LatitudeSign ? 'S' : 'N',
                    Course = bitCoordinate.Course * 1.41,
                    HDop = bitCoordinate.HDop,
                    Speed = bitCoordinate.Speed,
                    SatelliteCount = bitCoordinate.SatelliteCount
                };
            }

            shipMessage.Coordinates = coordinates;
            return shipMessage;
        }
    }
}