﻿using System;
using System.Collections.Generic;
using DirWatcher.SatShipLib.Abstract;
using DirWatcher.SatShipLib.Models.Bits;
using StxLib;

namespace DirWatcher.SatShipLib.Concrete
{
    public class SatShipConverter : ISatShipConverter
    {
        private readonly BitFields m_bitFields;

        public SatShipConverter(BitFields bitFields)
        {
            m_bitFields = bitFields ?? throw new ArgumentNullException(nameof(bitFields));
        }

        public BitMessage ToBitMessage(byte[] hexBytes)
        {
            if (hexBytes == null) throw new ArgumentNullException(nameof(hexBytes));

            m_bitFields.Initialize(hexBytes);

            var title = new BitTitle
            {
                MessageType = (byte)m_bitFields.GetNextField(3),
                Version = (byte)m_bitFields.GetNextField(4),
                Temperature = (byte)m_bitFields.GetNextField(7),
                Battery = (byte)m_bitFields.GetNextField(8)
            };

            var majorCoordinate = new BitCoordinate
            {
                Course = (byte)m_bitFields.GetNextField(8),
                Speed = (byte)m_bitFields.GetNextField(6),
                SatelliteCount = (byte)m_bitFields.GetNextField(5),
                DTime = 0,  //потому как последняя к передачи коодината
                HDop = (byte)m_bitFields.GetNextField(5),
                LongitudeSign = Convert.ToBoolean(m_bitFields.GetNextField(1))
            };

            title.MajorTime = (uint)m_bitFields.GetNextField(17);

            majorCoordinate.Latitude = (uint)m_bitFields.GetNextField(23);
            majorCoordinate.LatitudeSign = Convert.ToBoolean((byte)m_bitFields.GetNextField(1));
            majorCoordinate.Longitude = (uint)m_bitFields.GetNextField(24);

            var coordinates = new List<BitCoordinate> { majorCoordinate };

            for (byte coordinateIndex = 0; coordinateIndex < 2; coordinateIndex++)
            {
                var crd = new BitCoordinate
                {
                    Latitude = (uint)m_bitFields.GetNextField(23),
                    LatitudeSign = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    Longitude = (uint)m_bitFields.GetNextField(24),
                    LongitudeSign = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    DTime = (ushort)m_bitFields.GetNextField(15),
                    Course = (byte)m_bitFields.GetNextField(8),
                    Speed = (byte)m_bitFields.GetNextField(6),
                    SatelliteCount = (byte)m_bitFields.GetNextField(5),
                    HDop = (byte)m_bitFields.GetNextField(5)
                };
                //insert next position after a major(1) coordinate
                coordinates.Insert(1, crd);
            }

            return new BitMessage
            {
                Title = title,
                BitCoordinates = coordinates
            };
        }
    }
}