﻿using System;

namespace DirWatcher.SatShipLib.Models
{
    public struct SatShipMessage
    {
        public byte Temperature { get; set; }
        public double Battery { get; set; }
        public DateTime MessageTime { get; set; }

        public ShipCoordinate[] Coordinates { get; set; }
    }
}