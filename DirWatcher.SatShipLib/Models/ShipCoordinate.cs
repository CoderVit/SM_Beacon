﻿using System;

namespace DirWatcher.SatShipLib.Models
{
    public struct ShipCoordinate
    {
        public double Latitude { get; set; }
        public char LatitudeSign { get; set; }
        public double Longitude { get; set; }
        public char LongitudeSign { get; set; }
        public DateTime Time { get; set; }
        public double Course { get; set; }
        public byte Speed { get; set; }
        public byte SatelliteCount { get; set; }
        public byte HDop { get; set; }
    }
}