﻿namespace DirWatcher.SatShipLib.Models.Bits
{
    public struct BitTitle
    {
        public byte MessageType { get; set; }
        public byte Version { get; set; }
        public byte Temperature { get; set; }
        public byte Battery { get; set; }
        public uint MajorTime { get; set; }
    }
}