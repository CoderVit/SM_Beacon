﻿namespace DirWatcher.SatShipLib.Models.Bits
{
    public struct BitCoordinate
    {
        public uint Latitude { get; set; }
        public bool LatitudeSign { get; set; }
        public uint Longitude { get; set; }
        public bool LongitudeSign { get; set; }
        public ushort DTime { get; set; }
        public byte Course { get; set; }
        public byte Speed { get; set; }
        public byte SatelliteCount { get; set; }
        public byte HDop { get; set; }
    }
}