﻿using System.Collections.Generic;

namespace DirWatcher.SatShipLib.Models.Bits
{
    public struct BitMessage
    {
        public BitTitle Title { get; set; }
        public List<BitCoordinate> BitCoordinates { get; set; }
    }
}