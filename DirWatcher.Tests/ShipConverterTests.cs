﻿using System.Linq;
using DirWatcher.SatShipLib.Abstract;
using DirWatcher.SatShipLib.Concrete;
using StxLib;
using StxLib.Converters;
using Xunit;

namespace DirWatcher.Tests
{
    public class ShipConverterTests
    {
        private readonly SatShipConverter m_satShipConverter = new SatShipConverter(new BitFields());

        [Fact]
        private void CoordinateParseTest()
        {
            var bitConverter = new StxByteConverter();
            var bytes = bitConverter.ToBytes("0x81E621009098A706B52C33C87422B62C33C47422C21200C05ABB2C33CF7422AE1200C05A");
            var bitMessage = m_satShipConverter.ToBitMessage(bytes);

            Assert.Equal(135, bitMessage.Title.Battery);
            Assert.Equal((uint)3407, bitMessage.Title.MajorTime);

            var majorCoordinate = bitMessage.BitCoordinates.First();

            Assert.Equal(0, majorCoordinate.DTime);
            Assert.Equal((uint)3353781, majorCoordinate.Latitude);
            Assert.Equal((uint)2258120, majorCoordinate.Longitude);

            var firstCoordinate = bitMessage.BitCoordinates[1];

            Assert.Equal(2391, firstCoordinate.DTime);
            Assert.Equal((uint)3353787, firstCoordinate.Latitude);
            Assert.Equal((uint)2258127, firstCoordinate.Longitude);

            var lastCoordinate = bitMessage.BitCoordinates.Last();

            Assert.Equal(2401, lastCoordinate.DTime);
            Assert.Equal((uint)3353782, lastCoordinate.Latitude);
            Assert.Equal((uint)2258116, lastCoordinate.Longitude);
        }

        private readonly IMessageConverter m_messageConverter = new ShipMessageConverter(new SatShipConverter(new BitFields()), new BatteryConverter(), new TimeConvertor(), new CoordinateConverter());

        [Fact]
        private void ParseBitCoordinates()
        {
            var bitConverter = new StxByteConverter();
            var bytes = bitConverter.ToBytes("0x01F02100300D762885002E6ECC7400000000000000000000000000000000000000000000");
            var shipMessage = m_messageConverter.ToShipMessage(bytes, 1528003323);
        }
    }
}