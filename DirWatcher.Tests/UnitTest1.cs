using DirWatcher.SatProtBeaconLib;
using DirWatcher.SatProtBeaconLib.Concrete;
using StxLib;
using StxLib.Converters;
using StxLib.Models.Xml;
using Xunit;

namespace DirWatcher.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Success_Parse()
        {
            var message = new SndMessageConverter(new PayloadConverter(new StxByteConverter(), new CoordinateConverter(), new BitFields()), new TimeConvertor()).Convert(new StuMessage
            {
                Payload = new Payload
                {
                    Length = 18,
                    Value = "0xC0BB1C47330713550032438043F973B81F8E"
                }
            });

            Assert.Equal(56.009, message.coordinate.Latitude);
            Assert.Equal(92.92385, message.coordinate.Longitude);
        }
    }
}