﻿using System;
using DirWatcher.SatProtBeaconLib.Abstract;
using DirWatcher.SatProtBeaconLib.Models.Message;
using StxLib.Abstract;
using StxLib.Models.Xml;

namespace DirWatcher.SatProtBeaconLib
{
    public class SndMessageConverter
    {
        private readonly IPayloadConverter m_converter;
        private readonly ITime m_timeConverter;

        public SndMessageConverter(IPayloadConverter converter, ITime timeConverter)
        {
            m_converter = converter ?? throw new ArgumentNullException(nameof(converter));
            m_timeConverter = timeConverter ?? throw new ArgumentNullException(nameof(timeConverter));
        }

        public (DateTime time, Coordinate coordinate, Sensors sensors) Convert(StuMessage message)
        {
            var sndMessage = m_converter.PayloadToMessage(message.Payload);

            return (m_timeConverter.ToSatBeaconTime(message.UnixTime, sndMessage.title.DTime), sndMessage.coordinate, sndMessage.sensors);
        }
    }
}