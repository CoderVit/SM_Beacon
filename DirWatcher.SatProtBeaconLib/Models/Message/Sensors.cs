﻿namespace DirWatcher.SatProtBeaconLib.Models.Message
{
    public class Sensors
    {
        public int Temperature { get; set; }
        public double Battery { get; set; }
        public sbyte Hdop { get; set; }

        public Reasons Reasons { get; set; } = new Reasons();
        public Angles Angles { get; set; } = new Angles();     //Максимальный наклон в градусах, с момента последней передачи
    }
}