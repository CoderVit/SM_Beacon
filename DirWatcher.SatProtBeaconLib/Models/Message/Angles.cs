﻿using System;

namespace DirWatcher.SatProtBeaconLib.Models.Message
{
    public class Angles
    {
        public sbyte LastMaxZ { get; set; }  //Максимальный наклон в градусах, с момента последней передачи
        public ushort MaxX { get; set; }
        public ushort MaxY { get; set; }
        public ushort MaxZ { get; set; }

        public double ImpactForce
        {
            get
            {
                var x = MaxX / 64 + 512;
                var y = MaxY / 64 + 512;
                var z = MaxZ / 64 + 512;

                var res = Math.Sqrt(x * x + y * y + z * z);

                return 8 * res / short.MaxValue;
            }
        }
    }
}