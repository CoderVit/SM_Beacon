﻿namespace DirWatcher.SatProtBeaconLib.Models.Message
{
    public class Reasons
    {
        public bool IsActive { get; set; } //маяк работает в текущий момент
        public bool IsActiveLast24Hours { get; set; } //был факт работы маяка в течении предыдущих 24 часов
        public bool IsHit { get; set; } //сообщение передано по превышению заданного ускорения (по факту удара)
        public bool IsAngle { get; set; } //сообщение передано по превышению заданного угла наклона
        public bool IsDistance { get; set; } //сообщение передано по факту смещения координаты
        public bool IsLifeTime { get; set; } //сообщение передано времени сигнала жизни
    }
}