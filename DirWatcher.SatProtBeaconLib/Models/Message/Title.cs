﻿namespace DirWatcher.SatProtBeaconLib.Models.Message
{
    public class Title
    {
        public Title(byte messageType, byte version, uint dtime)
        {
            MessageType = messageType;
            Version = version;
            DTime = dtime;
        }

        public byte MessageType { get; }

        public byte Version { get; }

        public uint DTime { get; }
    }
}