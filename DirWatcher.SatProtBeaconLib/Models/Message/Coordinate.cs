﻿namespace DirWatcher.SatProtBeaconLib.Models.Message
{
    public class Coordinate
    {
        public double Latitude { get; set; }
        public char LatitudeSign { get; set; }

        public double Longitude { get; set; }
        public char LongitudeSign { get; set; }

        public byte Speed { get; set; }
        public byte Course { get; set; }
        public byte SatelliteCount { get; set; }
    }
}