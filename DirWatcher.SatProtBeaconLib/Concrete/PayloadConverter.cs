﻿using System;
using System.Globalization;
using DirWatcher.SatProtBeaconLib.Abstract;
using DirWatcher.SatProtBeaconLib.Models.Message;
using StxLib;
using StxLib.Abstract;
using StxLib.Models.Xml;

namespace DirWatcher.SatProtBeaconLib.Concrete
{
    public class PayloadConverter : IPayloadConverter
    {
        private readonly IByteConverter m_byteConverter;
        private readonly ICoordinateConvertor m_coordinateConvertor;
        private readonly BitFields m_bitFields;

        public PayloadConverter(IByteConverter byteConverter, ICoordinateConvertor coordinateConvertor, BitFields bitFields)
        {
            m_byteConverter = byteConverter ?? throw new ArgumentNullException(nameof(byteConverter));
            m_coordinateConvertor = coordinateConvertor ?? throw new ArgumentNullException(nameof(coordinateConvertor));
            m_bitFields = bitFields ?? throw new ArgumentNullException(nameof(bitFields));
        }

        public (Title title, Coordinate coordinate, Sensors sensors) PayloadToMessage(Payload payload)
        {
            if (payload.Length != 18) throw new Exception($"Payload length ({payload.Length} != 18)");

            var bytes = m_byteConverter.ToBytes(payload.Value);
            m_bitFields.Initialize(bytes);

            var title = GetTitle;
            var coordinate = GetCoordinate;
            var sensors = GetSensors;

            return (title, coordinate, sensors);
        }

        private Title GetTitle
        {
            get
            {
                var messageType = (byte)m_bitFields.GetNextField(3);
                var version = (byte)m_bitFields.GetNextField(2);
                var dTime = (uint)m_bitFields.GetNextField(11);

               return new Title(messageType, version, dTime);
            }
        }

        private Coordinate GetCoordinate
        {
            get
            {
                var latitude = (uint)m_bitFields.GetNextField(23);
                var latitudeSign = Convert.ToBoolean(m_bitFields.GetNextField(1));

                var longitude = (uint)m_bitFields.GetNextField(24);
                var longitudeSign = Convert.ToBoolean(m_bitFields.GetNextField(1));

                var speed = (byte)m_bitFields.GetNextField(5);
                var course = (byte)m_bitFields.GetNextField(5);
                var satelliteCount = (byte)m_bitFields.GetNextField(5);

                return new Coordinate
                {
                    Latitude = m_coordinateConvertor.ToLatitude(latitude),
                    LatitudeSign = latitudeSign ? 'S' : 'N',
                    Longitude = m_coordinateConvertor.ToLongitude(longitude),
                    LongitudeSign = longitudeSign ? 'W' : 'E',
                    Speed = speed,
                    Course = course,
                    SatelliteCount = satelliteCount
                };
            }
        }

        private Sensors GetSensors
        {
            get
            {
                var temperature = (sbyte)m_bitFields.GetNextField(7) - 64;
                var battery = (sbyte)m_bitFields.GetNextField(7);
                var hDop = (sbyte)m_bitFields.GetNextField(7);

                var reasons = new Reasons
                {
                    IsActive = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    IsActiveLast24Hours = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    IsHit = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    IsAngle = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    IsDistance = Convert.ToBoolean(m_bitFields.GetNextField(1)),
                    IsLifeTime = Convert.ToBoolean(m_bitFields.GetNextField(1))
                };

                var angles = new Angles
                {
                    LastMaxZ = (sbyte)m_bitFields.GetNextField(7),
                    MaxX = (ushort)m_bitFields.GetNextField(10),
                    MaxY = (ushort)m_bitFields.GetNextField(10),
                    MaxZ = (ushort)m_bitFields.GetNextField(10)
                };

                return new Sensors
                {
                    Temperature = temperature,
                    Battery = Convert.ToDouble(battery.ToString().Insert(1, "."), CultureInfo.InvariantCulture),
                    Hdop = hDop,
                    Reasons = reasons,
                    Angles = angles
                };
            }
        }
    }
}