﻿using DirWatcher.SatProtBeaconLib.Models.Message;
using StxLib.Models.Xml;

namespace DirWatcher.SatProtBeaconLib.Abstract
{
    public interface IPayloadConverter
    {
        (Title title, Coordinate coordinate, Sensors sensors) PayloadToMessage(Payload payload);
    }
}