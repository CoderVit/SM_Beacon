﻿using System;
using System.IO;
using System.Threading.Tasks;
using DirWatcher.Client.Clients;
using DirWatcher.Common.Abstract;
using DirWatcher.MessageManager.Abstract;
using DirWatcher.MessageManager.Concrete.Creators;
using DirWatcher.MessageManager.Converters;
using DirWatcher.MessageManager.Models;
using DirWatcher.SatProtBeaconLib;
using DirWatcher.Service.Abstract;
using Serilog;

namespace DirWatcher.Service.Concrete
{
    public class BeaconWatcher : IWatch
    {
        private readonly FileSystemWatcher m_fileSystemWatcher;
        private readonly IXmlReader m_xmlReader;
        private readonly SndMessageConverter m_messageConverter;
        private readonly IMessageSender m_messageSender;
        private readonly MessageFactory m_messageFactory;
        private readonly DeviceClient m_deviceServiceClient;

        public BeaconWatcher(Func<string, FileSystemWatcher> serviceAccessor, IXmlReader xmlReader, SndMessageConverter messageConverter, IMessageSender messageSender, MessageFactory messageFactory, DeviceClient deviceServiceClient)
        {
            m_fileSystemWatcher = serviceAccessor(nameof(BeaconWatcher)) ?? throw new ArgumentNullException(nameof(serviceAccessor));
            m_xmlReader = xmlReader ?? throw new ArgumentNullException(nameof(xmlReader));
            m_messageConverter = messageConverter ?? throw new ArgumentNullException(nameof(messageConverter));
            m_messageSender = messageSender ?? throw new ArgumentNullException(nameof(messageSender));
            m_messageFactory = messageFactory ?? throw new ArgumentNullException(nameof(messageFactory));
            m_deviceServiceClient = deviceServiceClient ?? throw new ArgumentNullException(nameof(deviceServiceClient));
        }

        #region IWatch implementation

        public void Watch()
        {
            m_fileSystemWatcher.EnableRaisingEvents = true;
            m_fileSystemWatcher.Created += (sender, args) => FileCreatedHanlder(args.FullPath);
        }

        public void Stop()
        {
            m_fileSystemWatcher.EnableRaisingEvents = false;
            m_fileSystemWatcher.Dispose();
        }
        #endregion IWatch implementation

        private void FileCreatedHanlder(string filePath)
        {
            Task.Run(async () =>
            {
                try
                {
                    var stuMessage = m_xmlReader.Read(filePath);

                    Log.Information($"{nameof(BeaconWatcher)}: {nameof(stuMessage.DeviceId)}:{stuMessage.DeviceId}, {nameof(stuMessage.UnixTime)}:{stuMessage.UnixTime} {nameof(stuMessage.Payload)}: {stuMessage.Payload.Value}");

                    var (dateTime, coordinate, sensors) = m_messageConverter.Convert(stuMessage);

                    var deviceId = TrimDeviceId(stuMessage.DeviceId);

                    var (deviceName, floatId) = await m_deviceServiceClient.GetGisDeviceAsync(stuMessage.DeviceId);

                    var gisMessage = m_messageFactory.CreateMessage(new SndMessage
                    {
                        DeviceId = deviceId,
                        DeviceName = deviceName,
                        FloatId = floatId,
                        MessageTime = dateTime,
                        Coordinate = coordinate,
                        Sensors = sensors
                    });

                    await m_messageSender.SendAsync(gisMessage);

                    //File.Delete(filePath);
                }
                catch (Exception exception)
                {
                    Log.Error(exception, $"{nameof(FileCreatedHanlder)} in {nameof(BeaconWatcher)}");
                }
            });
        }

        private long TrimDeviceId(string deviceId)
        {
            var splitted = deviceId.Substring(2, deviceId.Length - 2);
            return Convert.ToInt32(splitted);
        }
    }
}