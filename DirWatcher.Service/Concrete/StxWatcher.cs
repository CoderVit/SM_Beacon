﻿using System;
using System.IO;
using System.Threading.Tasks;
using DirWatcher.Client.Clients;
using DirWatcher.Common.Abstract;
using DirWatcher.Service.Abstract;
using MessageServiceReference;
using Serilog;
using StxLib.Abstract;

namespace DirWatcher.Service.Concrete
{
    internal class StxWatcher : IWatch
    {
        private readonly FileSystemWatcher m_fileSystemWatcher;
        private readonly IXmlReader m_xmlReader;
        private readonly IByteConverter m_byteConverter;
        private readonly MessageClient m_messageServiceClient;

        public StxWatcher(Func<string, FileSystemWatcher> serviceAccessor, IXmlReader xmlReader, IByteConverter byteConverter, MessageClient messageServiceClient)
        {
            m_fileSystemWatcher = serviceAccessor(nameof(StxWatcher)) ?? throw new ArgumentNullException(nameof(serviceAccessor));
            m_xmlReader = xmlReader ?? throw new ArgumentNullException(nameof(xmlReader));
            m_byteConverter = byteConverter ?? throw new ArgumentNullException(nameof(byteConverter));
            m_messageServiceClient = messageServiceClient ?? throw new ArgumentNullException(nameof(messageServiceClient));
        }

        #region IWatch Implementations

        public void Watch()
        {
            m_fileSystemWatcher.EnableRaisingEvents = true;
            m_fileSystemWatcher.Created += (sender, args) => FileCreatedHanlder(args.FullPath);
        }

        public void Stop()
        {
            m_fileSystemWatcher.EnableRaisingEvents = false;
            m_fileSystemWatcher.Dispose();
        }

        #endregion IWatch Implementations

        private void FileCreatedHanlder(string filePath)
        {
            Task.Run(async () =>
            {
                try
                {
                    var stuMessage = m_xmlReader.Read(filePath);

                    Log.Information($"{nameof(StxWatcher)}: {nameof(stuMessage.DeviceId)}:{stuMessage.DeviceId}, {nameof(stuMessage.UnixTime)}:{stuMessage.UnixTime} {nameof(stuMessage.Payload)}: {stuMessage.Payload.Value}");

                    if (stuMessage.Payload.Length != 36) throw new FormatException("Payload length(" + stuMessage.Payload.Length + ") != 36");

                    await m_messageServiceClient.AddMessageToDbAsync(new CoordinateMessage
                    {
                        DeviceId = stuMessage.DeviceId,
                        CoordinateTime = stuMessage.UnixTime,
                        MessageTime = File.GetCreationTime(filePath),
                        HexBytes = m_byteConverter.ToBytes(stuMessage.Payload.Value),
                        MessageType = MessageType.MESSAGE_TYPE_FTP,
                        CoordinateType = CoordinateType.COORDINATE_TYPE_SATELLITE,
                        ProtocolType = ProtocolType.PROTOCOL_TYPE_STX_3
                    });

                }
                catch (Exception exception)
                {
                    Log.Error(exception, $"{nameof(FileCreatedHanlder)} in {nameof(StxWatcher)}");
                }
            });
        }
    }
}