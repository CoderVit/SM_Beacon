﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DirWatcher.Client.Clients;
using DirWatcher.Client.Models;
using DirWatcher.Common.Abstract;
using DirWatcher.SatShipLib.Abstract;
using DirWatcher.Service.Abstract;
using MessageServiceReference;
using Serilog;
using StxLib.Abstract;

namespace DirWatcher.Service.Concrete
{
    internal class ShipWatcher : IWatch
    {
        private readonly FileSystemWatcher m_fileSystemWatcher;
        private readonly IByteConverter m_byteConverter;
        private readonly IXmlReader m_xmlReader;
        private readonly IMessageConverter m_messageConverter;
        private readonly MessageClient m_dbServiceClient;
        private readonly WsClient m_wsServiceClient;

        public ShipWatcher(Func<string, FileSystemWatcher> serviceAccessor, IByteConverter byteConverter, IXmlReader xmlReader, IMessageConverter messageConverter, MessageClient dbServiceClient, WsClient wsServiceClient)
        {
            m_fileSystemWatcher = serviceAccessor(nameof(ShipWatcher)) ?? throw new ArgumentNullException(nameof(serviceAccessor));
            m_byteConverter = byteConverter ?? throw new ArgumentNullException(nameof(byteConverter));
            m_xmlReader = xmlReader ?? throw new ArgumentNullException(nameof(xmlReader));
            m_messageConverter = messageConverter ?? throw new ArgumentNullException(nameof(messageConverter));
            m_dbServiceClient = dbServiceClient ?? throw new ArgumentNullException(nameof(dbServiceClient));
            m_wsServiceClient = wsServiceClient ?? throw new ArgumentNullException(nameof(wsServiceClient));
        }

        #region IWatch implementation

        public void Watch()
        {
            m_fileSystemWatcher.EnableRaisingEvents = true;
            m_fileSystemWatcher.Created += (sender, args) => FileCreatedHanlder(args.FullPath);
        }

        public void Stop()
        {
            m_fileSystemWatcher.EnableRaisingEvents = false;
            m_fileSystemWatcher.Dispose();
        }

        #endregion IWatch implementation

        private void FileCreatedHanlder(string filePath)
        {
            Task.Run(async () =>
            {
                try
                {
                    var stuMessage = m_xmlReader.Read(filePath);
                    var hexBytes = m_byteConverter.ToBytes(stuMessage.Payload.Value);

                    Log.Information($"{nameof(ShipWatcher)}: {nameof(stuMessage.DeviceId)}:{stuMessage.DeviceId}, {nameof(stuMessage.UnixTime)}:{stuMessage.UnixTime} {nameof(stuMessage.Payload)}: {stuMessage.Payload.Value}");

                    var shipMessage = m_messageConverter.ToShipMessage(hexBytes, stuMessage.UnixTime);

                    //add to our db
                    await m_dbServiceClient.AddMessagesToDbAsync(new CoordinateMessage
                    {
                        DeviceId = stuMessage.DeviceId,
                        CoordinateTime = stuMessage.UnixTime,
                        MessageTime = shipMessage.MessageTime,
                        HexBytes = hexBytes,
                        ProtocolType = ProtocolType.PROTOCOL_TYPE_SAT_SHIP_TERM,
                        CoordinateType = CoordinateType.COORDINATE_TYPE_SATELLITE,
                        MessageType = MessageType.MESSAGE_TYPE_FTP
                    }, new StxMessage
                    {
                        MessageTime = shipMessage.MessageTime,
                        Battery = shipMessage.Battery,
                        Temperature = shipMessage.Temperature,
                        Coordinates = shipMessage.Coordinates.Select(coordinate => new Coordinate
                        {
                            Latitude = coordinate.Latitude,
                            LatitudeSign = coordinate.LatitudeSign,
                            Longitude = coordinate.Longitude,
                            LongitudeSign = coordinate.LongitudeSign,
                            UnixDateTime = coordinate.Time
                        }).ToArray()
                    });

                    //send to deutch server
                    await m_wsServiceClient.SendAsync(new ShipDevice
                    {
                        DeviceId = stuMessage.DeviceId,
                        ShipCoordinate = shipMessage.Coordinates.Select(coordinate => new ShipCoordinate
                        {
                            Latitude = coordinate.Latitude,
                            Longitude = coordinate.Longitude,
                            Time = coordinate.Time,
                            Course = Convert.ToSingle(coordinate.Course),
                            Speed = coordinate.Speed,
                        }).ToArray()
                    });
                }
                catch (Exception exception)
                {
                    Log.Error(exception, $"{nameof(ShipWatcher)}");
                }
            });
        }
    }
}