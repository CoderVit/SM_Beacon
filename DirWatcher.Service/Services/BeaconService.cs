﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using DirWatcher.Service.Abstract;
using Serilog;

namespace DirWatcher.Service.Services
{
    internal class BeaconService : ServiceBase
    {
        private readonly IEnumerable<IWatch> m_watchers;

        public BeaconService(IEnumerable<IWatch> watchers)
        {
            m_watchers = watchers ?? throw new ArgumentNullException(nameof(watchers));
        }

        #region Overrides

        protected override void OnStart(string[] args)
        {
            foreach (var watcher in m_watchers)
            {
                try
                {
                    watcher.Watch();
                    Log.Information($"{watcher.GetType().Name} started");
                }
                catch (Exception exception)
                {
                    Log.Error(exception, nameof(OnStart));
                }
            }
            
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            foreach (var watcher in m_watchers)
            {
                watcher.Stop();
                Log.Information($"{watcher.GetType().Name} stopped");
            }
            
            base.OnStop();
        }
        #endregion Overrides
    }
}