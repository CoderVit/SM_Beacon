﻿using System;
using System.Collections.Generic;
using System.IO;
using DirWatcher.Client.Clients;
using DirWatcher.Common.Abstract;
using DirWatcher.Common.Concrete;
using DirWatcher.MessageManager.Abstract;
using DirWatcher.MessageManager.Abstract.Creators;
using DirWatcher.MessageManager.Concrete;
using DirWatcher.MessageManager.Concrete.Creators;
using DirWatcher.SatProtBeaconLib;
using DirWatcher.SatProtBeaconLib.Abstract;
using DirWatcher.SatProtBeaconLib.Concrete;
using DirWatcher.SatShipLib.Abstract;
using DirWatcher.SatShipLib.Concrete;
using DirWatcher.Service.Abstract;
using DirWatcher.Service.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Serilog;
using StxLib;
using StxLib.Abstract;
using StxLib.Converters;

namespace DirWatcher.Service
{
    internal class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IXmlReader, XmlReader>();
            services.AddTransient<IMessageBuilder, MessageBuilder>();
            services.AddTransient<SndMessageConverter>();

            services.AddTransient<IRequestCreator, RequestCreator>(serviceProvider =>
            {
                var uri = Configuration.GetSection("beaconWatcher:clientUri").Value;
                return new RequestCreator(uri);
            });

            services.AddTransient<IMessageSender, MessageSender>();
            services.AddTransient<MessageFactory>();

            #region Watchers
            services.AddTransient<IWatch, BeaconWatcher>();
            services.AddTransient<IWatch, ShipWatcher>();
            //services.AddTransient<IWatch, StxWatcher>();
            #endregion Watchers

            services.AddTransient<Func<string, FileSystemWatcher>>(provider => key =>
            {
                const string FILTER = "*.xml";
                switch (key)
                {
                    case nameof(BeaconWatcher):
                        var beaconWatchPath = Configuration.GetSection("beaconWatcher:path").Value;
                        var beaconWatcher = new FileSystemWatcher(beaconWatchPath, FILTER);
                        return beaconWatcher;
                    case nameof(ShipWatcher):
                        var shipWatchPath = Configuration.GetSection("shipWatcher:path").Value;
                        var shipWatcher = new FileSystemWatcher(shipWatchPath, FILTER);
                        return shipWatcher;
                    case nameof(StxWatcher):
                        var stxWatchPath = Configuration.GetSection("stxWatcher:path").Value;
                        var stxWatcher = new FileSystemWatcher(stxWatchPath, FILTER);
                        return stxWatcher;
                    default: throw new KeyNotFoundException();
                }
            });

           //services.AddDbContext<BeaconDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("defaultConnection")));

            #region Clients

            services.AddTransient<WsClient>();
            services.AddTransient<MessageClient>();
            services.AddTransient<DeviceClient>();

            #endregion Clients

            #region SatProtBeaconLib

            services.AddTransient<IPayloadConverter, PayloadConverter>();

            #endregion SatProtBeaconLib

            #region StxLib

            services.AddTransient<IXmlSerializer, StuMessageSerializer>();
            services.AddTransient<IByteConverter, StxByteConverter>();
            services.AddTransient<IBatteryConverter, BatteryConverter>();
            services.AddTransient<ICoordinateConvertor, CoordinateConverter>();
            services.AddTransient<ITime, TimeConvertor>();
            services.AddTransient<BitFields>();

            #endregion StxLib

            #region SatShipLib

            services.AddTransient<IMessageConverter, ShipMessageConverter>();
            services.AddTransient<ISatShipConverter, SatShipConverter>();

            #endregion SatShipLib

            JsonConvert.DefaultSettings = () =>
            {
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                settings.Converters.Add(new StringEnumConverter());
                return settings;
            };

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel
                .Information()
                .WriteTo
                .RollingFile(Path.Combine(Directory.GetCurrentDirectory(), Configuration.GetSection("logger:path").Value))
                .CreateLogger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app/*, BeaconDbContext dbContext*/)
        {
            //dbContext.Database.EnsureDeleted(); //delete db if exist
            //dbContext.Database.EnsureCreated(); //create db if not exist
        }
    }
}