﻿namespace DirWatcher.Service.Abstract
{
    public interface IWatch
    {
        void Watch();
        void Stop();
    }
}