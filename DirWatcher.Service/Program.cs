﻿using System.IO;
using System.Reflection;
using DirWatcher.Service.Abstract;
using DirWatcher.Service.Services;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System.ServiceProcess;
using Microsoft.AspNetCore.Hosting.WindowsServices;
using Microsoft.Extensions.DependencyInjection;

namespace DirWatcher.Service
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            var watchers = host.Services.GetServices<IWatch>();

            var beaconService = new BeaconService(watchers);
            
#if DEBUG
            host.RunAsService();
#else
            ServiceBase.Run(beaconService);
#endif
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseContentRoot(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))
                .UseStartup<Startup>()
                .Build();
    }
}