﻿using System.ComponentModel;

namespace DirWatcher.Service.Installers
{
    [RunInstaller(true)]
    public partial class ServiceInstaller: System.Configuration.Install.Installer
    {
        public ServiceInstaller()
        {
            InitializeComponent();
        }
    }
}