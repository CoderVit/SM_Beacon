﻿using System.ServiceProcess;

namespace DirWatcher.Service.Installers
{
    partial class ServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private ServiceProcessInstaller serviceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller beaconServiceInstaller;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.serviceProcessInstaller = new ServiceProcessInstaller();
            //
            // serviceProcessInstaller
            //
            this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstaller.Password = null;
            this.serviceProcessInstaller.Username = null;
            //
            // Iridium Service Installer
            //
            this.beaconServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.beaconServiceInstaller.DisplayName = "Directory Watcher Service";
            this.beaconServiceInstaller.ServiceName = "DirWatcherService";
            this.beaconServiceInstaller.Description = "Folders Watcher, handle incoming messages";
            this.beaconServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;   //Startup mode
            //
            // ProjectInstaller
            //
            this.Installers.Add(this.serviceProcessInstaller);
            this.Installers.Add(this.beaconServiceInstaller);
        }

        #endregion
    }
}