﻿using System;
using System.Threading.Tasks;
using DirWatcher.Client.Models;
using Serilog;
using WsServiceReference;

namespace DirWatcher.Client.Clients
{
    public class WsClient
    {
        private readonly DeviceClient m_deviceServiceClient = new DeviceClient();

        public async Task SendAsync(ShipDevice shipDevice)
        {
            var coordinates = shipDevice.ShipCoordinate;

            var pasttrackPositions = new PasttrackPosition[coordinates.Length];

            for (byte coordinateIndex = 0; coordinateIndex < coordinates.Length; coordinateIndex++)
            {
                var coordinate = coordinates[coordinateIndex];

                pasttrackPositions[coordinateIndex] = new PasttrackPosition
                {
                    lat = (int)(coordinate.Latitude * 60000),
                    lon = (int)(coordinate.Longitude * 60000),
                    timestamp = coordinate.Time,
                    cog = coordinate.Course,
                    sog = coordinate.Speed
                };
            }

            var vesselId = await m_deviceServiceClient.GetShipDeviceAsync(shipDevice.DeviceId);

            var report = new ReportOwnPositionRequest
            {
                body = new ReportOwnPositionRequestData
                {
                    apiIdentifier = new APIIdentifier
                    {
                        companyGuid = "b803c5ca-3a13-425d-b069-6bb1ad1aed87",
                        clientGuid = "563222fe-d8b3-475f-8ba1-fd527e590076"
                    },
                    deviceId = new DeviceId
                    {
                        keyreference = DeviceIdKeyreference.GUID,
                        mobileNo = shipDevice.DeviceId,
                        vesselID = vesselId
                    },
                    positions = pasttrackPositions
                }
            };

            var client = new WSPositionPortTypeClient();

            try
            {
                await client.OpenAsync();
                await client.ReportOwnPositionAsync(report);
                await client.CloseAsync();
            }
            catch (Exception exception)
            {
                Log.Error(exception, nameof(WsClient));
            }
            finally
            {
                client.Abort();
            }
        }
    }
}