﻿using System;
using System.Threading.Tasks;
using DeviceServiceReference;
using Serilog;

namespace DirWatcher.Client.Clients
{
    public class DeviceClient
    {
        public async Task<Tuple<string, long>> GetGisDeviceAsync(string deviceId)
        {
            var client = new DeviceServiceReference.DeviceServiceClient();
            try
            {
                await client.OpenAsync();
                var device = await client.GetGisDeviceIdAsync(deviceId);
                await client.CloseAsync();
                return device;
            }
            finally
            {
                client.Abort();
            }
        }

        public async Task<string> GetShipDeviceAsync(string deviceId)
        {
            var client = new DeviceServiceClient();
            try
            {
                await client.OpenAsync();
                var shipDevice = await client.GetShipDeviceIdAsync(deviceId);
                await client.CloseAsync();
                return shipDevice.ToString();
            }
            finally
            {
                client.Abort();
            }
        }
    }
}