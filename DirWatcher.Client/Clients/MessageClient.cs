﻿using System.Threading.Tasks;
using MessageServiceReference;

namespace DirWatcher.Client.Clients
{
    public class MessageClient
    {
        public async Task AddMessageToDbAsync(CoordinateMessage coordinateMessage)
        {
            var client = new MessageServiceClient();
            try
            {
                await client.OpenAsync();
                await client.AddMessageAsync(coordinateMessage);
                await client.CloseAsync();
            }
            finally
            {
               client.Abort();
            }
        }

        public async Task AddMessagesToDbAsync(CoordinateMessage coordinateMessage,  StxMessage message)
        {
            var client = new MessageServiceClient();
            try
            {
                await client.OpenAsync();
                await client.AddMessagesAsync(coordinateMessage, message);
                await client.CloseAsync();
            }
            finally
            {
                client.Abort();
            }
        }
    }
}