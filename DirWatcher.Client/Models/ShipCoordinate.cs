﻿using System;

namespace DirWatcher.Client.Models
{
    public class ShipCoordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime Time { get; set; }
        public float Course { get; set; }
        public float Speed { get; set; }
    }
}