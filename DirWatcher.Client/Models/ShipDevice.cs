﻿namespace DirWatcher.Client.Models
{
    public class ShipDevice
    {
        public string DeviceId { get; set; }

        public ShipCoordinate[]  ShipCoordinate { get; set; }
    }
}