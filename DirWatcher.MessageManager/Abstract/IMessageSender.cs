﻿using System.Threading.Tasks;
using DirWatcher.MessageManager.Models;

namespace DirWatcher.MessageManager.Abstract
{
    public interface IMessageSender
    {
        Task SendAsync(FloatMessage message);
    }
}