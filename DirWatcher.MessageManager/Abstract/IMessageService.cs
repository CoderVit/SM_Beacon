﻿namespace DirWatcher.MessageManager.Abstract
{
    public interface IMessageService
    {
        string GetGisId(string deviceId);
    }
}