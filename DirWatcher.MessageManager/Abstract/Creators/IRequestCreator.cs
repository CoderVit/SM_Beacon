﻿using System.Net;

namespace DirWatcher.MessageManager.Abstract.Creators
{
    public interface IRequestCreator
    {
        WebRequest Create(int length);
    }
}