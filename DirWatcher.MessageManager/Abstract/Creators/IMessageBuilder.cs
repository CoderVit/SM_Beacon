﻿using DirWatcher.MessageManager.Models;

namespace DirWatcher.MessageManager.Abstract.Creators
{
    public interface IMessageBuilder
    {
        FloatMessage CreateStateMessage(SndMessage sndMessage);
        FloatMessage CreateOffSetMessage(SndMessage sndMessage);
        FloatMessage CreateHitMessage(SndMessage sndMessage);
        FloatMessage CreateAngleMessage(SndMessage sndMessage);
    }
}