﻿using System;
using System.Net;
using DirWatcher.MessageManager.Abstract.Creators;

namespace DirWatcher.MessageManager.Concrete.Creators
{
    public class RequestCreator: IRequestCreator
    {
        private readonly string m_uri;
        private const string CONTENT_TYPE = "application/json";
        private const string TOKEN = "Basic YXZlcmluLnY6RmR0aGJ5";

        public RequestCreator(string uri)
        {
            m_uri = uri ?? throw new ArgumentNullException(nameof(uri));
        }

        public WebRequest Create(int length)
        {
            var request = WebRequest.Create(m_uri);
            request.Method = WebRequestMethods.Http.Put;
            request.ContentType = CONTENT_TYPE;
            request.Headers.Add("Authorization", TOKEN);
            request.ContentLength = length;

            return request;
        }
    }
}