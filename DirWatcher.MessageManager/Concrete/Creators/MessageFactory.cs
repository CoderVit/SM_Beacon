﻿using System;
using DirWatcher.MessageManager.Abstract.Creators;
using DirWatcher.MessageManager.Models;

namespace DirWatcher.MessageManager.Concrete.Creators
{
    public class MessageFactory
    {
        private readonly IMessageBuilder m_messageBuilder;

        public MessageFactory(IMessageBuilder messageBuilder)
        {
            m_messageBuilder = messageBuilder ?? throw new ArgumentNullException(nameof(messageBuilder));
        }

        public FloatMessage CreateMessage(SndMessage message)
        {
            var reasons = message.Sensors.Reasons;

            if (reasons.IsHit)
            {
                return m_messageBuilder.CreateHitMessage(message);
            }

            if (reasons.IsAngle)
            {
                return m_messageBuilder.CreateAngleMessage(message);
            }
            return reasons.IsDistance ? m_messageBuilder.CreateOffSetMessage(message) : m_messageBuilder.CreateStateMessage(message);
        }
    }
}