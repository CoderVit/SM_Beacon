﻿using System.Collections.Generic;
using DirWatcher.MessageManager.Abstract.Creators;
using DirWatcher.MessageManager.Models;
using DirWatcher.MessageManager.Models.Attribute;
using DirWatcher.MessageManager.Models.Enums;

namespace DirWatcher.MessageManager.Concrete.Creators
{
    public class MessageBuilder : IMessageBuilder
    {
        /// <summary>
        /// Создание сообщения состояния устройства
        /// </summary>
        /// <param name="sndMessage">Распарсенное сообщение прочтенное из файла</param>
        /// <returns>Сообщение возвращаемое РусГисом</returns>
        public FloatMessage CreateStateMessage(SndMessage sndMessage)
        {
            const byte BANK = 8;
            var message = new FloatMessage
            {
                Name = $"Вектор состояния изделия {sndMessage.DeviceName}",
                EntityTypeCodeName = TypeCodeName.FLOAT_COND,
                QualifierId = 257168815,
                Status = MessageStatus.ACTIVE,
                Attributes = new List<FloatAttribute>
                {
                    new NumericAttribute(FieldTypeCodeName.ID_FLOAT, sndMessage.DeviceId),
                    new NumericAttribute(FieldTypeCodeName.FLOAT_SPEED, sndMessage.Coordinate.Speed),
                    new NumericAttribute(FieldTypeCodeName.FLOAT_COURSE, sndMessage.Coordinate.Course),
                    new NumericAttribute(FieldTypeCodeName.FLOAT_VOLTAGE, sndMessage.Sensors.Battery),
                    new NumericAttribute(FieldTypeCodeName.FLOAT_BANK, BANK),
                    new NumericAttribute(FieldTypeCodeName.LAT, sndMessage.Coordinate.Latitude),
                    new NumericAttribute(FieldTypeCodeName.LON, sndMessage.Coordinate.Longitude),
                    new DateAttribute(FieldTypeCodeName.DATE_TIME_CHANGE, sndMessage.MessageTime),
                    new ReferenceAttribute(FieldTypeCodeName.FLOAT_ID, sndMessage.FloatId),
                    new ReferenceAttribute(FieldTypeCodeName.FLASHER, sndMessage.IsFlasherLight ? 254360735: 254360736 )
                }
            };
            return message;
        }
        /// <summary>
        /// Создание сообщения о смещении устройства
        /// </summary>
        /// <param name="sndMessage">Распарсенное сообщение прочтенное из файла</param>
        /// <returns>Сообщение возвращаемое РусГисом</returns>
        public FloatMessage CreateOffSetMessage(SndMessage sndMessage)
        {
            const int DISTANCE = 32;
            const int ALLOW_DISTANCE = 30;

            var message = new FloatMessage
            {
                Name = $"Снос {sndMessage.DeviceName}",
                EntityTypeCodeName = TypeCodeName.FLOAT_DMLTN_MSG,
                QualifierId = 257168722,
                Status = MessageStatus.ACTIVE,
                Attributes = new List<FloatAttribute>
                {
                    new NumericAttribute(FieldTypeCodeName.ID_FLOAT,sndMessage.DeviceId),
                    new NumericAttribute(FieldTypeCodeName.DEMLTN_DISTANCE, DISTANCE),
                    new DateAttribute(FieldTypeCodeName.TIME_MSG, sndMessage.MessageTime),
                    new NumericAttribute(FieldTypeCodeName.ALLOW_DEMOLTN_VAL, ALLOW_DISTANCE),
                    new ReferenceAttribute(FieldTypeCodeName.FLOAT_ID, sndMessage.FloatId)
                }
            };
            return message;
        }
        /// <summary>
        /// Создание сообщения об ударе устройства
        /// </summary>
        /// <param name="sndMessage">Распарсенное сообщение прочтенное из файла</param>
        /// <returns>Сообщение возвращаемое РусГисом</returns>
        public FloatMessage CreateHitMessage(SndMessage sndMessage)
        {
            var message = new FloatMessage
            {
                Name = $"Удар {sndMessage.DeviceName}",
                EntityTypeCodeName = TypeCodeName.FLOAT_IMPACT_MSG,
                QualifierId = 257168714,
                Status = MessageStatus.ACTIVE,
                Attributes = new List<FloatAttribute>
                {
                    new NumericAttribute(FieldTypeCodeName.ID_FLOAT, sndMessage.DeviceId),
                    new NumericAttribute(FieldTypeCodeName.IMPACT_FORCE, sndMessage.Sensors.Angles.ImpactForce),
                    new DateAttribute(FieldTypeCodeName.TIME_MSG, sndMessage.MessageTime),
                    new ReferenceAttribute(FieldTypeCodeName.FLOAT_ID, sndMessage.FloatId)
                }
            };
            return message;
        }
        /// <summary>
        /// Создание сообщения о превышении угла наклона устройства
        /// </summary>
        /// <param name="sndMessage">Распарсенное сообщение прочтенное из файла</param>
        /// <returns>Сообщение возвращаемое РусГисом</returns>
        public FloatMessage CreateAngleMessage(SndMessage sndMessage)
        {
            const byte ALLOW_ANGLE = 30;
            var message = new FloatMessage
            {
                Name = $"превышение угла {sndMessage.DeviceName}",
                EntityTypeCodeName = TypeCodeName.FLOAT_ANGLE_MSG,
                QualifierId = 257170345,
                Status = MessageStatus.ACTIVE,
                Attributes = new List<FloatAttribute>
                {
                    new NumericAttribute(FieldTypeCodeName.ID_FLOAT, sndMessage.DeviceId),
                    new DateAttribute(FieldTypeCodeName.TIME_MSG, sndMessage.MessageTime),
                    new NumericAttribute(FieldTypeCodeName.LON, sndMessage.Coordinate.Longitude),
                    new NumericAttribute(FieldTypeCodeName.LAT, sndMessage.Coordinate.Latitude),
                    new NumericAttribute(FieldTypeCodeName.ALLOW_TILT_ANGLE, ALLOW_ANGLE),
                    new NumericAttribute(FieldTypeCodeName.TILT_ANGLE, sndMessage.Sensors.Angles.LastMaxZ),
                    new DateAttribute(FieldTypeCodeName.DATE_TIME_CHANGE, sndMessage.MessageTime),
                    new ReferenceAttribute(FieldTypeCodeName.FLOAT_ID, sndMessage.FloatId)
                }
            };
            return message;
        }
    }
}