﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DirWatcher.MessageManager.Abstract;
using DirWatcher.MessageManager.Abstract.Creators;
using DirWatcher.MessageManager.Models;
using Newtonsoft.Json;
using Serilog;

namespace DirWatcher.MessageManager.Concrete
{
    public class MessageSender : IMessageSender
    {
        private readonly IRequestCreator m_requestCreator;

        public MessageSender(IRequestCreator requestCreator)
        {
            m_requestCreator = requestCreator ?? throw new ArgumentNullException(nameof(requestCreator));
        }

        public async Task SendAsync(FloatMessage message)
        {
            var json = JsonConvert.SerializeObject(message);

            var jsonBytes = Encoding.UTF8.GetBytes(json);

            var webRequest = m_requestCreator.Create(jsonBytes.Length);

            try
            {
                using (var sendStream = await webRequest.GetRequestStreamAsync())
                {
                    await sendStream.WriteAsync(jsonBytes, 0, jsonBytes.Length);
                }

                using (var response = await webRequest.GetResponseAsync())
                using (var responseStream = response.GetResponseStream())
                using (var streamReader = new StreamReader(responseStream ?? throw new InvalidOperationException(), Encoding.ASCII))
                {
                   // Log.Debug(streamReader.ReadToEnd());
                }
            }
            catch (Exception exception)
            {
                Log.Error(exception, nameof(SendAsync));
            }
        }
    }
}