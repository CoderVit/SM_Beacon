﻿using System;
using System.Collections.Generic;
using DirWatcher.MessageManager.Models.Attribute;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DirWatcher.MessageManager.Converters
{
    internal class AttributesConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var items = (IList<FloatAttribute>)value;

            var array = new JArray();
            foreach (var item in items)
            {
                var name = item.GetType().Name;
                var lowerName = char.ToLower(name[0]) + name.Substring(1);
                array.Add(new JObject(new JProperty(lowerName, JToken.FromObject(item, serializer))));
            }
            array.WriteTo(writer);
        }

        public override bool CanConvert(Type objectType)
        {
            // CanConvert is not called when the [JsonConverter] attribute is used
            return false;
        }
    }
}