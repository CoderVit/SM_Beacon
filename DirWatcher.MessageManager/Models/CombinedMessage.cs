﻿using System;
using DirWatcher.SatProtBeaconLib.Models.Message;

namespace DirWatcher.MessageManager.Models
{
    public class SndMessage
    {
        public long DeviceId { get; set; }
        public DateTime MessageTime { get; set; }
        public string DeviceName { get; set; }
        public long FloatId { get; set; }
        public bool IsFlasherLight { get; set; }
        public Coordinate Coordinate { get; set; } = new Coordinate();
        public Sensors Sensors { get; set; } = new Sensors();
    }
}