﻿namespace DirWatcher.MessageManager.Models.Enums
{
    public enum TypeCodeName
    {
        FLOAT_COND, //Вектор состояния
        FLOAT_DMLTN_MSG,    //Снос буя
        FLOAT_IMPACT_MSG,  //Удар буя
        FLOAT_ANGLE_MSG //превышение угла
    }
}