﻿namespace DirWatcher.MessageManager.Models.Enums
{
    public enum FieldTypeCodeName
    {
        ID_FLOAT,   //наш Id буя
        IMPACT_FORCE,   //Сила удара
        TIME_MSG,   //Дата и время сообщения
        DATE_TIME_CHANGE, //Дата и время сообщения
        FLOAT_ID,   //Id буя клиента
        FLOAT_SPEED,    //Скорость, м/с
        FLOAT_COURSE,   //Курс, градусы
        FLOAT_VOLTAGE,  //Напряжение батареи, вольт
        FLOAT_BANK,     //Крен буя-поплавка, градусы
        LAT,    //Широта
        LON,    //Долгота
        FLASHER,    //Статус работы проблескатора
        DEMLTN_DISTANCE,    //Расстояние сноса, м
        ALLOW_DEMOLTN_VAL,  //Допустимое расстояние сноса
        TILT_ANGLE,         //угол наклона
        ALLOW_TILT_ANGLE    //Допустимый угол наклона
    }
}