﻿using System.Collections.Generic;
using DirWatcher.MessageManager.Converters;
using DirWatcher.MessageManager.Models.Attribute;
using DirWatcher.MessageManager.Models.Enums;
using Newtonsoft.Json;

namespace DirWatcher.MessageManager.Models
{
    public class FloatMessage
    {
        public string Name { get; set; }
        public TypeCodeName EntityTypeCodeName { get; set; }
        public int QualifierId { get; set; }
        public MessageStatus Status { get; set; }

        [JsonConverter(typeof(AttributesConverter))]
        public IList<FloatAttribute> Attributes { get; set; }
    }
}