﻿using DirWatcher.MessageManager.Models.Enums;

namespace DirWatcher.MessageManager.Models.Attribute
{
    public class FloatAttribute
    {
        public FloatAttribute(FieldTypeCodeName codeName, object value)
        {
            EntityFieldTypeCodeName = codeName;
            Value = value;
        }

        public FieldTypeCodeName EntityFieldTypeCodeName { get; }
        public object Value { get; }
    }
}