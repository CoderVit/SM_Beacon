﻿using DirWatcher.MessageManager.Models.Enums;

namespace DirWatcher.MessageManager.Models.Attribute
{
    public class ReferenceAttribute : FloatAttribute
    {
        public ReferenceAttribute(FieldTypeCodeName codeName, object value) : base(codeName, value)
        {
        }
    }
}