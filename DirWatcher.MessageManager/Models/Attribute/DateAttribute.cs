﻿using DirWatcher.MessageManager.Models.Enums;

namespace DirWatcher.MessageManager.Models.Attribute
{
    public class DateAttribute : FloatAttribute
    {
        public DateAttribute(FieldTypeCodeName codeName, object value) : base(codeName, value)
        {
        }
    }
}