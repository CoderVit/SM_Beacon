﻿using DirWatcher.MessageManager.Models.Enums;

namespace DirWatcher.MessageManager.Models.Attribute
{
    public class NumericAttribute : FloatAttribute
    {
        public NumericAttribute(FieldTypeCodeName codeName, object value) : base(codeName, value)
        {
        }
    }
}