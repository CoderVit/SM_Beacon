﻿using StxLib.Models.Xml;

namespace DirWatcher.Common.Abstract
{
    public interface IXmlReader
    {
        StuMessage Read(string filePath);
    }
}