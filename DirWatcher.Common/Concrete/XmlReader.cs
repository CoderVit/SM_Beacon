﻿using System;
using System.IO;
using System.Threading;
using DirWatcher.Common.Abstract;
using StxLib.Abstract;
using StxLib.Models.Xml;

namespace DirWatcher.Common.Concrete
{
    public class XmlReader : IXmlReader
    {
        private readonly IXmlSerializer m_xmlSerializer;
        private readonly byte m_readCount;

        public XmlReader(IXmlSerializer xmlSerializer, byte readCount = 5)
        {
            m_xmlSerializer = xmlSerializer ?? throw new ArgumentNullException(nameof(xmlSerializer));
            m_readCount = readCount;
        }

        public StuMessage Read(string filePath)
        {
            if (!File.Exists(filePath)) throw new FileNotFoundException(nameof(filePath));

            byte tryingCount = 0;
            do
            {
                try
                {
                    var xmlString = File.ReadAllText(filePath);
                    return m_xmlSerializer.Deserialize(xmlString);
                }
                catch
                {
                    tryingCount++;
                    Thread.Sleep(1000);
                }
            } while (tryingCount < m_readCount);
            throw new InvalidOperationException("Can't read file");
        }
    }
}